import {UserModel} from "../models/user";
import EncryptionService from './encryption-service';
import DbService from './db-service';
import {v4 as uuid} from 'uuid';
import {MongoClient, Db} from "mongodb";
import EmailService from './email-service';
import {ProfileModel} from "../models/profile";
import {config} from "../config";
import CodeGeneratorService from './code-generator-service';
import {ChestModel} from "..//models/chest";
import {KeyModel} from "../models/key";

const ObjectId = require('mongodb').ObjectID;

class ProfileService {

    constructor() {

    }

    async find(characterName: string): Promise<ProfileModel> {
        const promise = new Promise<ProfileModel>(async(resolve, reject) => {

            let conn = null;

            try{
                conn = await DbService.connectToDB(config.dbUri);
                const foundUser = await conn.db.collection("users").findOne({
                    "profile.searchCharacterName": characterName.toLowerCase()
                });

                if(foundUser){
                    resolve(foundUser.profile);
                } else {
                    reject({message: "Could not find profile matching that character name"});
                }
            } catch(error) {
                reject(error);
            } finally {
                if(conn && conn.client) {
                    conn.client.close();
                }
            }
        });

        return promise;
    }

    async search(partialName: string) : Promise<Array<ProfileModel>> {
        const promise = new Promise<Array<ProfileModel>>(async(resolve, reject) => {
            let conn = null;

            try {
                conn = await DbService.connectToDB(config.dbUri);
                console.log('looking for:'+ partialName);
                const foundUsers = await conn.db.collection("users").find({
                    "profile.searchCharacterName": {
                        $regex: partialName.toLowerCase()
                    }
                }).toArray();

                console.log(foundUsers);
                if (foundUsers && foundUsers.length > 0) {
                    resolve(foundUsers.map(foundUser => foundUser.profile));
                } else {
                    resolve([]);
                }
            } catch (error) {
                reject(error);
            } finally {
                if(conn && conn.client) {
                    conn.client.close();
                }
            }
        });

        return promise;
    }

    async update(cacheKey: string, userId: string, newProfile: ProfileModel) {
        const promise = new Promise<ProfileModel>(async(resolve, reject) => {
            let conn = null;

            try{
                conn = await DbService.connectToDB(config.dbUri);
                const foundUser = await conn.db.collection("users").findOne({"_id": ObjectId(userId)});

                if(newProfile.characterName) {
                    const otherUsers = await conn.db.collection("users").find({
                        $and: [
                            {"profile.searchCharacterName": newProfile.characterName.toLowerCase()},
                            {
                                _id: {
                                    $ne: ObjectId(userId)
                                }
                            }
                        ]
                    }).toArray();

                    if(otherUsers && otherUsers.length > 0) {
                        reject("Another user already has this character name!");
                    }
                }

                if(foundUser){

                    const setFields = {
                        $set: {
                            "profile.characterLevel": newProfile.characterLevel,
                            "profile.characterDescription": newProfile.characterDescription,
                            "profile.characterName": newProfile.characterName,
                            "profile.searchCharacterName": newProfile.characterName.toLowerCase(),
                            "profile.avatarId": newProfile.avatarId
                        }
                    };


                    const result = await conn.db.collection("users").updateOne(
                        {"_id": ObjectId(userId)},
                        setFields
                    );

                    const updatedUser = await conn.db.collection("users").findOne({"_id": ObjectId(userId)});

                    resolve(updatedUser.profile);

                } else {
                    reject({message: "Unable to update profile"});
                }
            } catch(error) {
                reject(error);
            } finally {
                if(conn && conn.client) {
                    conn.client.close();
                }
            }
        });

        return promise;
    }

    async addChestKey(characterName: string, targetChest: ChestModel) {
        const promise = new Promise<ProfileModel>(async(resolve, reject) => {
            let conn = null;

            try{
                conn = await DbService.connectToDB(config.dbUri);

                const foundUser = await conn.db.collection("users").findOne({
                    "profile.searchCharacterName": characterName.toLowerCase()
                });

                if(foundUser){

                    const newKey = new KeyModel();
                    newKey.code = targetChest._id;
                    newKey.description = "";
                    newKey.for = targetChest.name;
                    newKey.name = "Guild key to " + targetChest.name;

                    const setFields = {
                        $push: {
                            "profile.keys": newKey
                        }
                    };

                    await conn.db.collection("users").updateOne(
                        {"_id": ObjectId(foundUser._id)},
                        setFields
                    );

                    const resultUser = await conn.db.collection("users").findOne({"_id": ObjectId(foundUser._id)});
                    resolve(resultUser);

                } else {
                    reject({message: "Unable to add key to profile"});
                }
            } catch(error) {
                reject(error);
            } finally {
                if(conn && conn.client) {
                    conn.client.close();
                }
            }
        });

        return promise;
    }
}

export default new ProfileService();