import * as mongodb from "mongodb";

class DbService {

    constructor(){

    }

    async connectToDB(uri: string): Promise<any> {
        const promise = new Promise(async (resolve, reject) => {

            try{
                const client = await mongodb.MongoClient.connect(uri, {useNewUrlParser: true});
                const db = client.db();
                resolve({client, db});
            } catch (error) {
                console.log(error);
                reject(error);
            }

        });

        return promise;
    }

}

export default new DbService();
