import * as http from "http";
import debug from "debug";
import App from "./app";
import DbService from "./services/db-service";
import {config} from "./config";

debug("ts-express:server");

const startServer = () => {
    const port = process.env.PORT || 8080;
    App.set('port', port);

    const server = http.createServer(App);
    server.listen(port);

    const onError = (error: NodeJS.ErrnoException) => {
        console.log("Something has gone terribly wrong!");
        console.error(error);
    };

    const onListening = () => {
        let addr = server.address();
        console.log(`Listening at ${addr.address}:${port}`);
    };

    server.on("error", onError);
    server.on("listening", onListening);
};

DbService.connectToDB(config.dbUri).then(
    (successDb) => {
            if (successDb.client) {
                successDb.client.close();
            }
            startServer();
        },
    (error) => {console.log('Unable to connect to DB, cancelling server-start')}
);