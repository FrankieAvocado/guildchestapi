import {UserModel} from "../models/user";
import * as crypto from 'crypto';
import {config} from "../config";
import DbService from "../services/db-service";
import EncryptionService from "../services/encryption-service";

class DbAuthCache {
    private keyMap;

    constructor(){
        this.keyMap = {};
    }

    async registerNewKey(user: UserModel): Promise<string>{
        const generatedKey = await this.createRandomKey();


        const promise = new Promise<string>(async(resolve, reject) => {

            let conn = null;

            try{
                conn = await DbService.connectToDB(config.dbUri);
                const updatedUser = await conn.db.collection("users").updateOne({
                        email: user.email
                    },
                    {
                        $set: {
                            token: generatedKey
                        }
                    }
                );

                resolve(generatedKey);

            } catch(error) {
                reject(error);
            } finally {
                if(conn && conn.client) {
                    conn.client.close();
                }
            }
        });

        return promise;
    }

    async getKeyForUser(user: UserModel): Promise<string>{

        const promise = new Promise<string>(async(resolve, reject) => {

            let conn = null;

            try{
                conn = await DbService.connectToDB(config.dbUri);
                const foundUser = await conn.db.collection("users").findOne({
                        email: user.email
                    },
                );

                resolve(foundUser.token);

            } catch(error) {
                reject(error);
            } finally {
                if(conn && conn.client) {
                    conn.client.close();
                }
            }
        });

        return promise

    }

    async removeKey(key: string) {
        const promise = new Promise<string>(async(resolve, reject) => {

            let conn = null;

            try{
                conn = await DbService.connectToDB(config.dbUri);
                const updatedUser = await conn.db.collection("users").updateOne({
                        token: key
                    },
                    {
                        $set: {
                            token: null
                        }
                    }
                );

                resolve();

            } catch(error) {
                reject(error);
            } finally {
                if(conn && conn.client) {
                    conn.client.close();
                }
            }
        });

        return promise
    }

    async confirmKey(key: string): Promise<UserModel>{
        const promise = new Promise<UserModel>(async(resolve, reject) => {

            let conn = null;

            try{
                conn = await DbService.connectToDB(config.dbUri);
                const foundUser = await conn.db.collection("users").findOne({
                        token: key
                    },
                );

                resolve(foundUser);

            } catch(error) {
                reject(error);
            } finally {
                if(conn && conn.client) {
                    conn.client.close();
                }
            }
        });

        return promise
    }

    async createRandomKey(): Promise<string> {
        const promise = new Promise<string>((resolve, reject) => {
            crypto.randomBytes(128, (err, buffer) => {
                const token = buffer.toString('hex');
                resolve(token);
            });
        });

        return promise;
    }
}

export default new DbAuthCache();