import express from 'express';
import UserService from '../services/user-service';
import UserMiddleware from '../middleware/user';
import DbAuth from "../dbauth";

export class UsersRoutes {

    constructor(){}

    registerRoutes(router: express.Router): void {
        router.get('/users/public-profile', (req, res) => {
            res.status(200).send('<p>Hello, from public profile!</p>');
        });

        router.get('/users/private-profile', UserMiddleware.requireAuthenticated, (req, res) => {
            res.status(200).send(`<p>Hello ${req['user'].username}, from private profile!</p>`);
        });

        router.post('/users/sign-up', async (req, res) => {
            const password = req.body.password;
            const email = req.body.email;
            try
            {
                const result = await UserService.signup(email, password);
                const newUser = await UserService.findUser(result.toString());
                newUser.confirmationCode = '';
                const key = await DbAuth.registerNewKey(newUser);
                res.status(200).json({
                    user: newUser,
                    key
                });
            } catch(e) {
                console.log('everything asplode!', JSON.stringify(e, null, 2));
                res.status(500).json(e);
            }
        })
    }

}