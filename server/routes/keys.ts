import express from 'express';
import UserMiddleware from '../middleware/user';
import ChestService from '../services/chest-service';
import UserService from '../services/user-service';
import ProfileService from '../services/profile-service';
import {UserModel} from "../models/user";

export class KeysRoutes {

    constructor() {
    }

    registerRoutes(router: express.Router): void {

        router.post("/keys/craft", UserMiddleware.requireAuthenticated, async(req, res) => {

            try {
                const currentUser = req["user"] as UserModel;
                const targetChestId = req.body.chestId;
                const targetUsername = req.body.username;

                const foundChest = await ChestService.getChestDetails(targetChestId, currentUser);
                const foundProfile = await ProfileService.find(targetUsername);

                if(foundProfile && foundProfile.keys
                    && foundProfile.keys.some(x => x.code.toString() === targetChestId.toString())) {
                    throw "This user already has access to this chest!";
                }

                if(currentUser.profile.keys.some(x => x.code.toString() === foundChest._id.toString())) {
                    const updatedProfile =
                        await ProfileService.addChestKey(targetUsername, foundChest);

                    res.status(200).json(foundChest);
                } else {
                    res.status(401).json({message: "You don't have permission to create keys for this chest!"});
                }

            } catch(error) {
                res.status(500).json(error);
            }
        });

    }
}