import {ProfileModel} from "./profile";

export class GuildModel {
    _id: string;
    name: string;
    members: Array<ProfileModel>;
}