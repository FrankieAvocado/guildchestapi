export class KeyModel {
    name: string;
    for: string;
    description: string;
    code: string;
}