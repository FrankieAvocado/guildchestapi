import {ProfileModel} from "./profile";

export class UserModel {
    _id: string;
    email: string;
    safeword: string;
    signUpDate: Date;
    confirmedDate: Date;
    confirmationCode: string;
    profile: ProfileModel;
    token: string;
    tokenExpiration: Date;
}