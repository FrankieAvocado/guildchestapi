export class ChestLogEvent {
    _id: string;
    description: string;
    occurredOn: Date;
}

export class StoredItem {
    name: string;
    searchableName: string;
    count: number;
    appraisedValue: number;
    iconId: number;
}

export class ChestModel {
    _id: string;
    name: string;
    log: Array<ChestLogEvent>;
    contents: Array<StoredItem>;
    gold: number;
}